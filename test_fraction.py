import pytest
from fraction import Fraction

@pytest.fixture(scope = "module")
def setup():
    frc = Fraction()
    print("\n Setup Function")
    yield
    print("\n Teardown Function")

# Instantiate the Class
def test_can_instantiate_fraction():
    frc_no_value_passed = Fraction()
    frc_one_value_passed = Fraction(1)
    frc_one_value_specified_with_variable = Fraction(den = 10)
    frc_both_value_passed = Fraction(-1, 2)
    assert frc_no_value_passed
    assert frc_one_value_passed
    assert frc_one_value_specified_with_variable
    assert frc_both_value_passed

# Test gcd method in Fraction class
def test_gcd_method():
    frc = Fraction(1, 2)
    assert frc.gcd(1, 4) == 1

# Test lowest_method
def test_lowest_method():
    frc = Fraction()
    assert frc.lowest(30, 70, 1) == (3, 7)

# Test add_fraction method
def test_add_fraction_method():
    frc1 = Fraction(1, 2)
    frc2 = Fraction(1, 3)
    assert frc1.addFraction(frc2) == (5, 6)


